var rootURL="";

$(function($){
    var addToAll = false;
    var gallery = true;
    $(addToAll ? 'img' : 'img.fancybox').each(function(){
        var $this = $(this);
        var title = $this.attr('title');
        var src = $this.attr('data-big') || $this.attr('src');
        var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
        $this.wrap(a);
    });
    if (gallery)
        $('a.fancybox').attr('rel', 'fancyboxgallery');
    pagination();
    // initPrevImgs();
});
// $.noConflict();

function pagination(){
    var verImgCnt=3;
    var horImgCnt=3;
    $.ajax({
      type: 'GET',
      url: rootURL+'/pagination', 
      dataType: "json",
      success: function(data){
        var list = data == null ? [] : (data.total_img_cnt instanceof Array ? data.total_img_cnt : [data.total_img_cnt]);
        var pageCnt=Math.ceil(list[0]/(verImgCnt*horImgCnt));
        for(var i=0; i<pageCnt; i++){
            var ele = $('<li/>').html(i+1)
                                .attr('value',(i+1))
                                .click(function(){
                                    $('.pagination').children().removeClass();
                                    $(this).addClass('active');
                                    initPrevImgs(this);
                                });
            if(i==0){
                ele.addClass('active');
            }
            $('.pagination').append(ele);
        }
        $('.pagination li:nth-child(1)').click()
      },
      error:function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus);
      }
    });
}

function initPrevImgs(page){
    $('#ul_id').html("");
    $.ajax({
      type: 'GET',
      url: rootURL+'/initImages'+'/'+page.value, 
      dataType: "json",
      success: function(data){
        var list = data == null ? [] : (data.prev_imgs instanceof Array ? data.prev_imgs : [data.prev_imgs]);
        
        // $('#div_id').append('<img src="data:image/jpg;base64,' + data + '" />');
        // var div_id = document.getElementById("div_id");
        // div_id.innerHTML='<img src="data:image/jpg;base64,' + list[1] + '" />';

        var img_title="";
        for(var i=0; i<list.length; i++){
          //list짝수번 인덱스는 이미지 이름, 홀수번인덱스는 이미지 실제위치 데이터임~
          if(i%2==0){
            img_title = list[i];
          }
          else if(i%2==1){
            var ul_id=document.getElementById('ul_id');
            var li_element = document.createElement("li");

            li_element.innerHTML='<figure class="img-wrapper fade"><a class="fancybox" href="'+list[i]+'" rel="fancyboxgallery"><img src="' + list[i] + '" width="250" height="200" name="'+img_title+'"></a><h4>'+img_title+'</h4></figure>';
            ul_id.appendChild(li_element);
          }


        }
        
        setTitlePosition();

      },
      error:function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus);
      }
    });
    
}
function setTitlePosition(){
    var titlePosition = 'inside';
    $('a.fancybox').fancybox({
        titlePosition: titlePosition
    });
}

function setImgTagAttr(){
    var imgsrc = $("#fancybox-img").attr("src").toString();
    var fileNameArr = imgsrc.split("/");
    var fileName = fileNameArr[fileNameArr.length-1];
    var downImgLoc = imgsrc.replace("preview_images","");
    $("#fancybox-download").attr("href", downImgLoc);
    $("#fancybox-download").attr("download", fileName);
}
/*다운로드 클릭이벤트 리스너*/
// jquery.fancybox-1.3.4.pack.min.js안에 HTML코드로 구현되어있음. 거기서 아래 function 콜함
function downloadImage(){
    /*a 태그 내의 href, download속성을 설정해주는 방법*/
    setImgTagAttr();

    /*직접 dataURL을 String으로 넘겨받아 저장하는 방법*/
    // var imgsrc = $("#fancybox-img").attr("src").toString();
    // var fileNameArr = imgsrc.split("/");
    // var fileName = fileNameArr[fileNameArr.length-1];
    // // alert(rootURL+'/downloadImage'+'/'+fileName);
    // $.ajax({
    //   type: 'GET',
    //   url: rootURL+'/downloadImage'+'/'+fileName, 
    //   dataType: "json",
    //   success: function(data){
    //     var list = data == null ? [] : (data.down_imgs instanceof Array ? data.down_imgs : [data.down_imgs]);
    //     var dataURL = list[0];
    //     document.location = 'data:Application/octet-stream;base64,' + dataURL;
        
    //     /*방법1*/
    //     // function onDownload() {
    //     //   document.location = 'data:Application/octet-stream,' +
    //     //   encodeURIComponent(dataToDownload);
    //     // }
    //     /*방법2*/
    //     // data:image/jpg;base64,
    //     // try {
    //     //   localStorage.setItem("test_down_img1", dataURL);
    //     // }
    //     // catch (e) {
    //     //   alert("Storage failed: " + e);
    //     // }
    //   },
    //   error:function(jqXHR, textStatus, errorThrown){
    //         alert('error: ' + textStatus);
    //   }
    // });
}
// $('#fileupload').fileupload({
//     url : rootURL+'/uploadImage', 
//     dataType: 'json',
//     add: function (e, data) {
//         data.context = $('<button/>').text('Upload')
//         .appendTo($('#fileUpload_div'))
//         .click(function () {
//             data.context = $('<p/>').text('Uploading...').replaceAll($(this));
//             data.submit();
//         });
//     },
//     done: function (e, data) {
//         data.context.text('Upload finished.');
//     },
//     progressall: function (e, data) {
//         var progress = parseInt(data.loaded / data.total * 30, 10);
//         $('#progress .bar').css(
//             'width',
//             progress + '%'
//         );
//     }
// });


// 파일업로드부분
$(function () {
    //한번에 한 파일만 올릴 수 있게~
    $('#addFileBtn').click(function(){
        $('#files').html('');
        $('#progress').show();
    });

    var currentFileIdx=0;
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '/gallery/imgs/',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .css('margin-top', '5px')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .css('display','inline-block')
                    .append($('<span/>').attr('id','imgName').text(file.name))
                    .append('<br>')
                    .append($('<input/>').attr('type','text').attr('id','uploadText').attr('placeholder','설명').attr('width','50px;').css({
                        'width':'20vw',
                        'height':'50px'}));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width', progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        // 설명데이터뽑아서서버로보내는함수
        postImageAttr();
        // this써서 해당 div 삭제하는 코드 삽입(업로드완료후 해당 부분 지워주는코드)
        // $('#files :eq('+currentFileIdx+')').html('');
        $('#uploadText').remove();
        $('#imgName').html('업로드 완료');

        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $('#uploadText').remove();
        $('#imgName').html('업로드 실패');

        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $('#progress .progress-bar').css(
                'width', '0'
            );
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function postImageAttr(){
    $.ajax({
        type: 'POST',
        url: rootURL+'/uploadImageAttr', 
        data: formToJSON(),
        dataType:"json",
        contentType: false,
        processData: false,
        success: function(data){
            var list = data == null ? [] : (data.post_imgs instanceof Array ? data.post_imgs : [data.post_imgs]);
            alert(list[0]);
        },
        error:function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus);
        }
    });
}
function formToJSON(){
    var date = new Date();
    var postDate = '';
    postDate+=date.getFullYear()+"-";
    if(1<=date.getMonth() && date.getMonth()<10){
        postDate+='0'+date.getMonth()+'-';
    }
    else{
        postDate+=date.getMonth()+'-';
    }
    if(1<=date.getDate() && date.getDate()<10){
        postDate+='0'+date.getDate()+' ';
    }
    else{
        postDate+=date.getDate()+' ';
    }
    if(1<=date.getHours() && date.getHours()<10){
        postDate+='0'+date.getHours()+':';
    }
    else{
        postDate+=date.getHours()+':';
    }
    if(1<=date.getMinutes() && date.getMinutes()<10){
        postDate+='0'+date.getMinutes()+':';
    }
    else{
        postDate+=date.getMinutes()+':';
    }
    if(1<=date.getSeconds() && date.getSeconds()<10){
        postDate+='0'+date.getSeconds();
    }
    else{
        postDate+=date.getSeconds();
    }
    var postText = '';
    if($('#uploadText').val().length==0){
        postText = 'null';
    }
    else{
        postText = $('#uploadText').val();
    }

    alert($('#imgName').html()+"/"+postText+"/"+postDate);
    return JSON.stringify({
        "imgName": $('#imgName').html(),
        "imgDesc": postText,
        "imgPostDate": postDate
        });
}

// function fileUploadFunc(imgData){
//     var files = imgData.files;
//     var formData = new FormData();
//     formData.append('image', files[0]);

//     for(var i=0; i<files.length; i++){
//         alert('Upload file "'+files[0].name+'"');
//     }

//     $.ajax({
//         type: 'POST',
//         url: rootURL+'/uploadImage', 
//         data: "formData",
//         dataType:"json",
//         contentType: false,
//         processData: false,
//         success: function(data){
//             var list = data == null ? [] : (data.post_imgs instanceof Array ? data.post_imgs : [data.post_imgs]);
//             alert(list[0]);
//         },
//         error:function(jqXHR, textStatus, errorThrown){
//             alert('error: ' + textStatus);
//         }
//     });
//     // // This code is only for demo ...
//     // console.log("name : " + file.name);
//     // console.log("size : " + file.size);
//     // console.log("type : " + file.type);
//     // console.log("date : " + file.lastModified);
// }

/*삭제 클릭이벤트 리스너*/
// jquery.fancybox-1.3.4.pack.min_manager.js안에 HTML코드로 구현되어있음. 거기서 아래 function 콜함
function deleteImage(){
    var imgsrc = $("#fancybox-img").attr("src").toString();
    var fileNameArr = imgsrc.split("/");
    var fileName = fileNameArr[fileNameArr.length-1];
    alert('Delete file "'+fileName+'"');

    $.ajax({
        type: 'DELETE',
        url: rootURL+'/deleteImage'+'/'+fileName, 
        dataType:"json",
        success: function(data){
            alert("success");
        },
        error:function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus);
        }
    });
}