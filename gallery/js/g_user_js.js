var rootURL="";

$(function($){
    var addToAll = false;
    var gallery = true;
    $(addToAll ? 'img' : 'img.fancybox').each(function(){
        var $this = $(this);
        var title = $this.attr('title');
        var src = $this.attr('data-big') || $this.attr('src');
        var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
        $this.wrap(a);
    });
    if (gallery)
        $('a.fancybox').attr('rel', 'fancyboxgallery');
    pagination();
    initImgList();
});
// $.noConflict();

function pagination(){
    var verImgCnt=3;
    var horImgCnt=3;
    $.ajax({
      type: 'GET',
      url: rootURL+'/pagination', 
      dataType: "json",
      success: function(data){
        var list = data == null ? [] : (data.total_img_cnt instanceof Array ? data.total_img_cnt : [data.total_img_cnt]);
        var pageCnt=Math.ceil(list[0]/(verImgCnt*horImgCnt));
        for(var i=0; i<pageCnt; i++){
            var ele = $('<li/>').html(i+1);
            if(i==0){
                ele.addClass('active');
            }
            $('.pagination').append(ele);
        }
      },
      error:function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus);
      }
    });
}

function initImgList(){
    $.ajax({
      type: 'GET',
      url: rootURL+'/initImages', 
      dataType: "json",
      success: function(data){
        var list = data == null ? [] : (data.prev_imgs instanceof Array ? data.prev_imgs : [data.prev_imgs]);
        
        // $('#div_id').append('<img src="data:image/jpg;base64,' + data + '" />');
        // var div_id = document.getElementById("div_id");
        // div_id.innerHTML='<img src="data:image/jpg;base64,' + list[1] + '" />';

        var img_title="";
        for(var i=0; i<list.length; i++){
          //list짝수번 인덱스는 이미지 이름, 홀수번인덱스는 이미지 실제위치 데이터임~
          if(i%2==0){
            img_title = list[i];
          }
          else if(i%2==1){
            var ul_id=document.getElementById('ul_id');
            var li_element = document.createElement("li");

            li_element.innerHTML='<figure class="img-wrapper fade"><a class="fancybox" href="'+list[i]+'" rel="fancyboxgallery"><img src="' + list[i] + '" width="250" height="200" name="'+img_title+'"></a><h4>'+img_title+'</h4></figure>';
            ul_id.appendChild(li_element);
          }


        }
        
        setTitlePosition();

      },
      error:function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus);
      }
    });

    
}
function setTitlePosition(){
    var titlePosition = 'inside';
    $('a.fancybox').fancybox({
        titlePosition: titlePosition
    });
}

function setImgTagAttr(){
    var imgsrc = $("#fancybox-img").attr("src").toString();
    var fileNameArr = imgsrc.split("/");
    var fileName = fileNameArr[fileNameArr.length-1];
    var downImgLoc = imgsrc.replace("preview_images","original_images");
    $("#fancybox-download").attr("href", downImgLoc);
    $("#fancybox-download").attr("download", fileName);
}
/*다운로드 클릭이벤트 리스너*/
// jquery.fancybox-1.3.4.pack.min.js안에 HTML코드로 구현되어있음. 거기서 아래 function 콜함
function downloadImage(){
    /*a 태그 내의 href, download속성을 설정해주는 방법*/
    setImgTagAttr();

    /*직접 dataURL을 String으로 넘겨받아 저장하는 방법*/
    // var imgsrc = $("#fancybox-img").attr("src").toString();
    // var fileNameArr = imgsrc.split("/");
    // var fileName = fileNameArr[fileNameArr.length-1];
    // // alert(rootURL+'/downloadImage'+'/'+fileName);
    // $.ajax({
    //   type: 'GET',
    //   url: rootURL+'/downloadImage'+'/'+fileName, 
    //   dataType: "json",
    //   success: function(data){
    //     var list = data == null ? [] : (data.down_imgs instanceof Array ? data.down_imgs : [data.down_imgs]);
    //     var dataURL = list[0];
    //     document.location = 'data:Application/octet-stream;base64,' + dataURL;
        
    //     /*방법1*/
    //     // function onDownload() {
    //     //   document.location = 'data:Application/octet-stream,' +
    //     //   encodeURIComponent(dataToDownload);
    //     // }
    //     /*방법2*/
    //     // data:image/jpg;base64,
    //     // try {
    //     //   localStorage.setItem("test_down_img1", dataURL);
    //     // }
    //     // catch (e) {
    //     //   alert("Storage failed: " + e);
    //     // }
    //   },
    //   error:function(jqXHR, textStatus, errorThrown){
    //         alert('error: ' + textStatus);
    //   }
    // });
}