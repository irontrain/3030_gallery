<?php
require 'php/Slim/Slim.php';

Slim\Slim::registerAutoloader();
// UploadHandler\UploadHandler::registerAutoloader();

$app = new \Slim\Slim();

$response = $app->response();
$response->header('Access-Control-Allow-Origin', '*');

$app->get('/pagination', 'getTotalImageCnt');
$app->get('/initImages/:page', 'getPreviewImages');
$app->get('/downloadImage/:fileName', 'getImageFile');
// $app->post('/uploadImageAttr', 'postImageAttr');
$app->post('/uploadImageAttr', function() use($app){
	$request = $app->request();
	$imgAttr = json_decode($request->getBody());

	/*파일IO시작(DB구현시 삭제 예정)*/
	$myfile = fopen("g_imgAttrTest.txt", "w") or die("Unable to open file!");
	$txt = $imgAttr->imgName . ' / ';
	fwrite($myfile, $txt);
	$txt = $imgAttr->imgDesc . ' / ';;
	fwrite($myfile, $txt);
	$txt = $imgAttr->imgPostDate . ' / ';;	
	fwrite($myfile, $txt);
	fclose($myfile);
	/*파일IO끝*/

	// mysqli 트랜잭션
	// $link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	$result = mysqli_query($link, "INSERT INTO...");
	/*나중에사용*/
	// $imgAttr->imgName;
	// $imgAttr->imgPostDate;
	// $imgAttr->imgDesc;
	mysqli_commit($link);
});
$app->delete('/deleteImage/:fileName', 'deleteImageFile');


// $app->put('/wines/:id','updateWine');
// $app->delete('/wines/:id', 'deleteWine');

$app->run();

function getTotalImageCnt(){
	$totalImgCnt=20;
	echo '{"total_img_cnt": ' . $totalImgCnt . '}';
}
function getPreviewImages($page){
	$totalImgCnt=20;
	// 1~9
 	// 10~18
 	// 19~27
    $firstImgIdx = 1+(($page-1)*9);
    $lastImgIdx = 9*$page;
    if($lastImgIdx>$totalImgCnt){
    	$lastImgIdx = $totalImgCnt;
    }
	/*
	이미지 여러개 넣는 경우
	*/	
	$img_arr= array();
	for($i=$firstImgIdx;  $i<=$lastImgIdx; $i++){
		$img_name = "img".($i);
		$img_loc = "/gallery/imgs/files/preview_images/img".($i).".jpg";
		//짝수번 인덱스는 이미지 이름([0],[2],[4]..)
		array_push($img_arr, $img_name);
		//홀수번 인덱스는 이미지 실제 위치([1],[3],[5]..)
		array_push($img_arr, $img_loc);
	}
	echo '{"prev_imgs": ' . json_encode($img_arr) . '}';

	/*이미지 자체로 넣는 경우*/
	// $img_data = file_get_contents("gallery/images/small1.jpg");
	// header('Content-type:image/jpg');
	// echo base64_encode($img_data);
}

function getImageFile($fileName){
	$img_data = file_get_contents("gallery/imgs/files/".$fileName);
	header('Content-type:image/jpg');
	echo '{"down_imgs": ' . json_encode(base64_encode($img_data)) . '}';
	// echo $fileName;
}

function deleteImageFile($fileName){

}

// function postImageAttr(){
	
// 	$request = $app->request();
// 	$imgAttr = json_decode($request->getBody());
// 	/*파일IO시작*/
// 	$myfile = fopen("imgAttrTest.txt", "w") or die("Unable to open file!");
// 	$txt = 'aa';
// 	fwrite($myfile, $txt);
// 	$txt = 'aa';
// 	fwrite($myfile, $txt);
// 	$txt = 'aa';	
// 	fwrite($myfile, $txt);
// 	fclose($myfile);
// 	/*파일IO끝*/
	

// 	// mysqli 트랜잭션
// 	// $link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
// 	if(mysqli_connect_errno()){
// 		printf("Connect failed: %s",mysqli_connect_errno());
// 		exit();
// 	}
// 	mysqli_begin_transaction($link);

// 	$result = mysqli_query($link, "INSERT INTO...");
// 	/*나중에사용*/
// 	// $imgAttr->imgName;
// 	// $imgAttr->imgPostDate;
// 	// $imgAttr->imgDesc;
// 	mysqli_commit($link);






// 	// if (!isset($_FILES['uploads'])) {
// 	// 	echo '{"post_imgs": ' . json_encode("No files uploaded!!") . '}';
// 	// 	return;
// 	// }
// 	// $imgs = array();

// 	// $files = $_FILES['uploads'];
// 	// $cnt = count($files['name']);

// 	// for($i = 0 ; $i < $cnt ; $i++) {
// 	// 	if ($files['error'][$i] === 0) {
// 	// 		$name = uniqid('img-'.date('Ymd').'-');
// 	// 		if (move_uploaded_file($files['tmp_name'][$i], 'uploads/' . $name) === true) {
// 	// 			$imgs[] = array('url' => '/uploads/' . $name, 'name' => $files['name'][$i]);
// 	// 		}

// 	// 	}
// 	// }

// 	// $imageCount = count($imgs);

// 	// if ($imageCount == 0) {
// 	// 	echo '{"post_imgs": ' . json_encode('No files uploaded!!  <p><a href="/">Try again</a>') . '}';
// 	// 	return;
// 	// }

// 	// $plural = ($imageCount == 1) ? '' : 's';

// 	// foreach($imgs as $img) {
// 	// 	printf('%s <img src="%s" width="50" height="50" /><br/>', $img['name'], $img['url']);
// 	// }
// }
















function getPortalUser($year, $month, $day){
	/*Path던..Portal이던.. 1월+2월+3월..수 =전체기간수가 왜 다른가? ->쿼리에서 중복된걸 제거하는데.. 달이 달라지면 중복된걸 뺴주지 못하고 다시 카운트 하기 때문(쿼리를 새롭게 날리기 떄문에 DISTINCT가 중복을 잡아내지 못한다.. )*/
	if(1<=$month&&$month<10){
		$month = "0".$month;
	}
	if(1<=$day&&$day<10){
		$day = "0".$day;
	}
	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//각 변수들 선언 및 카운팅
	$portal_arr= array();
	array_push($portal_arr,'portal_naver');
	array_push($portal_arr,'portal_daum');
	array_push($portal_arr,'portal_google');
	array_push($portal_arr,'portal_etc');
	array_push($portal_arr,'total_cnt');

	$query_str_arr=array();
	//년도, 월 선택 안된 경우(전체기간)
	//referer 필드에서 naver를 포함하는 데이터의 갯수(중복제거)
	$query_str_arr[0]="SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%naver%'";
	//referer 필드에서 daum을 포함하는 데이터의 갯수(중복제거)
	$query_str_arr[1]="SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%daum%'";
	//referer 필드에서 google을 포함하는 데이터의 갯수(중복제거)
	$query_str_arr[2]="SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%google%'";
	//referer 필드에서 기타경로 데이터의 갯수(중복제거)
	$query_str_arr[3]="SELECT count(distinct referer) FROM homepagelog WHERE referer NOT LIKE '%naver%' AND referer NOT LIKE '%daum%' AND referer NOT LIKE '%google%'";
	//referer 필드에서 총 데이터의 갯수(중복제거)
	$query_str_arr[4]="SELECT count(distinct referer) FROM homepagelog";

	//년도만 선택된 경우
	if($year!='no' && $month=='no'){
		for($i=0; $i<count($query_str_arr); $i++){
			if($i==count($query_str_arr)-1){
				$query_str_arr[$i]=$query_str_arr[$i]. " WHERE left(postdate,4)='".$year."'";
				continue;
			}
			$query_str_arr[$i]=$query_str_arr[$i]. " AND left(postdate,4)='".$year."'";
		}
	}

	//년도, 월 모두 선택된 경우
	if($year!='no' && $month!='no'){
		//일(day)이 선택되지 않은 경우
		if($day=='all'){
			for($i=0; $i<count($query_str_arr); $i++){
				if($i==count($query_str_arr)-1){
					$query_str_arr[$i]=$query_str_arr[$i]. " WHERE left(postdate,7)='".$year."-".$month."'";
					continue;
				}
				$query_str_arr[$i]=$query_str_arr[$i]. " AND left(postdate,7)='".$year."-".$month."'";
			}
		}
		//일(day)이 선택된 경우
		else{
			for($i=0; $i<count($query_str_arr); $i++){
				if($i==count($query_str_arr)-1){
					$query_str_arr[$i]=$query_str_arr[$i]. " WHERE left(postdate,10)='".$year."-".$month."-".$day."'";
					continue;
				}
				$query_str_arr[$i]=$query_str_arr[$i]. " AND left(postdate,10)='".$year."-".$month."-".$day."'";
			}
		}
	}

	for($i=0; $i<count($portal_arr); $i++){
		$result = mysqli_query($link, $query_str_arr[$i]);
		mysqli_commit($link);
		$row = mysqli_fetch_assoc($result);
		$portal_arr[$i] = (int)$row['count(distinct referer)'];
	}

	//기타포털 접속자수는 그냥 토탈접속자수에서 각 포털별 접속자수 빼주고 남은걸로 해버리기 
	$portal_arr[3]=$portal_arr[4] - ($portal_arr[0]+$portal_arr[1]+$portal_arr[2]);

	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	$arr = array();
	for($i=0; $i<count($portal_arr);$i++){
		$arr[$i] = $portal_arr[$i];
	}
	echo '{"portal": ' . json_encode($arr) . '}';
}



////아직구현안함//////

function addWine(){
	// global $global_year, $global_month;
	echo "$app->post('/wines', 'addWine');";
}

function updateWine($id){
	// global $global_year, $global_month;
	echo "$app->put('/wines/:id','updateWine');";
}
?>